/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mukku.albumproject.dao;

import com.mukku.albumproject.helper.DatabaseHelper;
import com.mukku.albumproject.model.SaleReport;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author acer
 */
public class SaleDao {

    public List<SaleReport> getDayReport() {
        ArrayList<SaleReport> list = new ArrayList();
//        String sql = "SELECT strftime(\"%Y-%m-%d\",InvoiceDate) as period, SUM(Total) as Total FROM invoices\n"
//                + "GROUP BY period\n"
//                + "ORDER BY period DESC;";
        String sql = "SELECT strftime(\"%Y-%m-%d\",InvoiceDate) as period, SUM(Total) as Total FROM invoices\n"
                + "LEFT JOIN invoice_items\n"
                + "ON Invoices.InvoiceId = invoice_items.InvoiceId\n"
                + "GROUP BY period\n"
                + "ORDER BY period DESC;";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                SaleReport product = SaleReport.fromRS(rs);
                list.add(product);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<SaleReport> getMonthReport() {
        ArrayList<SaleReport> list = new ArrayList();
        String sql = "SELECT strftime(\"%Y-%m\",InvoiceDate) as period, SUM(Total) as Total FROM invoices\n"
                + "LEFT JOIN invoice_items\n"
                + "ON Invoices.InvoiceId = invoice_items.InvoiceId\n"
                + "GROUP BY period\n"
                + "ORDER BY period DESC;";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                SaleReport product = SaleReport.fromRS(rs);
                list.add(product);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<SaleReport> getMonthReport(int year) {
        ArrayList<SaleReport> list = new ArrayList();
        String sql = "SELECT strftime(\"%Y-%m\",InvoiceDate) as period, SUM(Total) as Total FROM invoices\n"
                + "LEFT JOIN invoice_items\n"
                + "ON Invoices.InvoiceId = invoice_items.InvoiceId\n"
                + "WHERE strftime(\"%Y\",InvoiceDate)=\"" + year + "\"\n"
                + " GROUP BY period\n"
                + "ORDER BY period DESC;";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                SaleReport product = SaleReport.fromRS(rs);
                list.add(product);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
}
