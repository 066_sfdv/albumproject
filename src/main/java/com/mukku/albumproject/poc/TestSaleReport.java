/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mukku.albumproject.poc;

import com.mukku.albumproject.model.SaleReport;
import com.mukku.albumproject.service.ReportService;
import java.util.List;

/**
 *
 * @author acer
 */
public class TestSaleReport {

    public static void main(String[] args) {
        ReportService reportService = new ReportService();
        List<SaleReport> reportDay = reportService.saleReportByDay();
//        for (SaleReport s : reportDay) {
//            System.out.println(s);
//        }
        List<SaleReport> reportMonth = reportService.saleReportByMonth();
//        for (SaleReport s : reportMonth) {
//            System.out.println(s);
//        }

        List<SaleReport> reportMonthC = reportService.saleReportByMonth(2013);
        for (SaleReport s : reportMonthC) {
            System.out.println(s);
        }
    }
}
