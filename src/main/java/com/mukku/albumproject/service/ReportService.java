/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mukku.albumproject.service;

import com.mukku.albumproject.dao.SaleDao;
import com.mukku.albumproject.model.SaleReport;
import java.util.List;

/**
 *
 * @author acer
 */
public class ReportService {

    public List<SaleReport> saleReportByDay() {
        SaleDao saleDao = new SaleDao();
        return saleDao.getDayReport();
    }

    public List<SaleReport> saleReportByMonth() {
        SaleDao saleDao = new SaleDao();
        return saleDao.getMonthReport();
    }

    public List<SaleReport> saleReportByMonth(int year) {
        SaleDao saleDao = new SaleDao();
        return saleDao.getMonthReport(year);
    }
}
